#include <Arduino.h>
#include <ESP8266WiFi.h>
extern "C"
{
#include "user_interface.h" //biblioteca para ler e escrever na RTC
#include <espnow.h>         //biblioteca esp_now
}

//Adafruit LIS3DH
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_LIS3DH_1.h>
#include <Adafruit_Sensor.h>

//DEFINES
#define SLEEPTIME 5e6   //tempo de sleep normal
#define SLEEPTIME2 15e6 //tempo de sleep se estiver parado
#define GET 98776       //codigo para ver se o boot é para ler o sensor
#define SEND 74893      //codigo para ver se o boot é para ler os valores da RTC
#define RTCMEMSTART 66  //endereço onde vamos começar a escrever as structs 64+2. No endereço 64 e 65 vai ficar a rtcManagement
#define RTCMEMORYLEN 125
#define MAXREADINGS 6 //numero de structs que vai mandar de cada vez
#define CHANNEL 6     //channel para o esp_now, tem que ser igual ao channel do WiFi
//#define LIS_RANGE LIS3DH_RANGE_4_G    //Range do acc
//#define LIS_DR LIS3DH_DATARATE_100_HZ //Datarate do acc

//VARIAVEIS GLOBAIS
//Esta struct é sempre escrita e lida no inicio (endereço 64)
typedef struct
{                   //struct que ajuda na leitura da rtc
  int codigo;       //codigo de boot
  int valueCounter; //numero de dados escritos na rtc
} rtcManagementStruc;

rtcManagementStruc rtcManagement;

struct LIS3DH_DATA
{ //struct com os dados do sensor
  float x;
  float y;
  float z;
} rtcMem;

int buckets;                                                                                                        //numero de pacotes de 4 bytes que vai ocupar a LIS3DH_DATA
uint8_t macMaster[] = {0xCC, 0x50, 0xE3, 0xC7, 0x67, 0x80};                                                         //MAC address do esp8266 "master"
uint8_t macSlave[] = {0x3E, 0x71, 0xBF, 0x2A, 0xD9, 0x23};                                                          //MAC address do esp8266 "slave"
uint8_t kok[16] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66}; //chave para a chave
uint8_t key[16] = {0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44}; //chave para a comunicação esp_now

//FUNÇÕES
void print_data(LIS3DH_DATA *obj);
void initWiFi();               //Inicializa o WiFi
void initEspNow();             //Inicializa o protocolo ESP_NOW
void sendReading(LIS3DH_DATA); //Envia as leituras por ESP_NOW
LIS3DH_DATA getReading();      //Lê o sensor
void initLIS3DH();

//LIS3DH init
int intPin = D5;
Adafruit_LIS3DH acc = Adafruit_LIS3DH();

void setup()
{
  pinMode(D0, WAKEUP_PULLUP);
  Serial.begin(115200);

  //Init do acc
  initLIS3DH();
  //Modo de deteção de rest
  acc.setRestDetection();

  //Calculo do numero de buckets de LIS3DH_DATA
  buckets = (sizeof(rtcMem) / 4);

  //Vamos ver que em que modo é que o esp deu boot --> GET ou SEND ou INITIAL VALUES NOT SET
  system_rtc_mem_read(64, &rtcManagement, sizeof(rtcManagement));
  Serial.print("Booting in mode: ");
  if (rtcManagement.codigo == GET)
  {
    Serial.println("GET");
  }
  else if (rtcManagement.codigo == SEND)
  {
    Serial.println("SEND");
  }
  else
  {
    Serial.println("INITIAL VALUES NOT SET");
  }

  //Se não for nenhum dos modos significa que é o primeiro boot e temos de meter os valores iniciais
  if (rtcManagement.codigo != GET && rtcManagement.codigo != SEND)
  {
    rtcManagement.codigo = GET;
    rtcManagement.valueCounter = 0;
    system_rtc_mem_write(64, &rtcManagement, sizeof(rtcManagement));
    Serial.println("Initial values set! Booting with code GET...");
    ESP.deepSleep(10, WAKE_NO_RFCAL);
    //ESP.deepSleep(0, WAKE_NO_RFCAL);
  }

  //Se o codigo for GET temos de ler o sensor
  if (rtcManagement.codigo == GET)
  {
    //Se o numero de valores for menor que 8
    if (rtcManagement.valueCounter < MAXREADINGS)
    {
      Serial.println("Reading the sensor...");
      rtcMem = getReading();
      print_data(&rtcMem);

      int rtcPos = RTCMEMSTART + rtcManagement.valueCounter * buckets; //Posição onde vai escrever na RTC aumenta cada vez que um valor novo é escrito (valueCounter)
      system_rtc_mem_write(rtcPos, &rtcMem, sizeof(rtcMem));
      Serial.printf("Value written in RTC in %d \n", rtcPos);
      rtcManagement.valueCounter++;

      //Atualizar a rtcManagment
      system_rtc_mem_write(64, &rtcManagement, sizeof(rtcManagement));
      Serial.println(digitalRead(intPin));
      if (digitalRead(intPin) == 1)
      {
        Serial.println("ACC parado... Going to sleep for 10 seconds");
        ESP.deepSleep(SLEEPTIME2, WAKE_NO_RFCAL);
      }
      else
      {
        Serial.println("GET routine complete! Going to sleep for 4 seconds");
        ESP.deepSleep(SLEEPTIME, WAKE_NO_RFCAL); //NO_RFCAL serve para não fazer a calibração do WiFi
      }
    }
    else
    { //Se o numero de valores escritos na RTC já for maior que 8 então temos de preparar a rotina de SEND
      rtcManagement.codigo = SEND;
      rtcManagement.valueCounter = 0;
      system_rtc_mem_write(64, &rtcManagement, sizeof(rtcManagement));
      Serial.println("Booting in SEND mode...");
      //ESP.deepSleep(0, WAKE_RFCAL);
      ESP.deepSleep(10, WAKE_RFCAL); //WAKE_RFCAL serve para quando der boot vai preparar o WiFi
    }
  }
  else
  { //Se o codigo for SEND
    initWiFi();
    initEspNow();

    Serial.println("Sending values!");
    for (int i = 0; i < MAXREADINGS; i++)
    {
      int rtcPos = RTCMEMSTART + i * buckets;
      system_rtc_mem_read(rtcPos, &rtcMem, sizeof(rtcMem));
      sendReading(rtcMem);
    }
    Serial.println("SEND routine complete!");

    //Depois de ler os valores todos temos de preparar o boot em modo GET
    rtcManagement.codigo = GET;
    rtcManagement.valueCounter = 0;
    system_rtc_mem_write(64, &rtcManagement, sizeof(rtcManagement));
    Serial.print("Booting in GET mode");
    ESP.deepSleep(10, WAKE_NO_RFCAL);
    //ESP.deepSleep(0, WAKE_NO_RFCAL);
  }
}

void loop() {} //No loop não precisa de fazer nada porque funciona com os deepSleeps

void print_data(LIS3DH_DATA *obj)
{
  Serial.print(obj->x);
  Serial.print('\t');
  Serial.print(obj->y);
  Serial.print('\t');
  Serial.println(obj->z);
}

void initEspNow()
{
  if (esp_now_init() != 0)
  {
    Serial.println("*** ESP_now init failed ***");
    ESP.restart();
  }
  delay(1); //vi na net que este delay ajudava https://github.com/HarringayMakerSpace/IoT/blob/master/ESP-Now/espnow-sensorNode/espnow-sensorNode.ino

  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);

  //Configuração da encryptação
  esp_now_set_kok(kok, 16);
  esp_now_add_peer(macSlave, ESP_NOW_ROLE_SLAVE, CHANNEL, key, 16); //adds slave to peer list
  esp_now_set_peer_key(macSlave, key, 16);

  esp_now_register_send_cb([](uint8_t *mac, uint8_t sendStatus) {    //callback para saber se a struct foi enviada
    Serial.printf("send_cb, send done, status = %i \n", sendStatus); //sendStatus =0 --> Sucesso / sendStatus =1 -->Falhou
  });
}

void initWiFi()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin();
  Serial.println();
  Serial.print("Mac Address in Station: ");
  Serial.println(WiFi.macAddress().c_str());
}

void sendReading(LIS3DH_DATA sd)
{
  u8 bs[sizeof(sd)];
  memcpy(bs, &sd, sizeof(sd));
  esp_now_send(NULL, bs, sizeof(bs));
  delay(20);
}

LIS3DH_DATA getReading()
{
  sensors_event_t event;
  acc.getEvent(&event);
  struct LIS3DH_DATA *newdata = (struct LIS3DH_DATA *)malloc(sizeof(struct LIS3DH_DATA));

  newdata->x = event.acceleration.x;
  newdata->y = event.acceleration.y;
  newdata->z = event.acceleration.z;

  return *newdata;
}

void initLIS3DH()
{
  Serial.println("LIS3DH starting!");

  if (!acc.begin(0x18))
  { // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1)
      ;
  }
  Serial.println("LIS3DH found!");
}